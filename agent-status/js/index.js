//globals
var error401Text = '<div class="ajax-data"><span class="text-warning">Error: not logged in. <a href="/labconfig/login/">Click here</a> to log in.</span></p>';
var register = {};

function ajaxFailCheck(xhr) {
	if(xhr.status == 401) { 
		window.location.reload();
	}
}
//Get the size of an object
Object.size = function(obj) {
	var size = 0, key;
	for (key in obj) {
		if (obj.hasOwnProperty(key)) size++;
	}
	return size;
};

function substringMatcher(strs) {
	return function findMatches(q, cb) {
		var matches, substringRegex;

		// an array that will be populated with substring matches
		matches = [];

		// regex used to determine if a string contains the substring `q`
		substrRegex = new RegExp(q, 'i');

		// iterate through the pool of strings and for any string that
		// contains the substring `q`, add it to the `matches` array
		$.each(strs, function(i, str) {
			if (substrRegex.test(str)) {
				matches.push(str);
			}
		});

		cb(matches);
	};
}

function replaceDnisSpan(name){
	if(typeof cache.demoDnis === 'undefined') {
		$('span.phone-number.' + name).html('<i class="fa fa-spinner fa-spin"></i>');
		$.ajax(config.api.demoDnis())
		.done(function(rsp){
			console.log("demoDnis response: ",rsp);
			cache.demoDnis = rsp;
			processDnis(rsp);
		});
	} else {
		processDnis(cache.demoDnis);	
	}
	function processDnis(rsp) {
		$.each(rsp,function(i,dnis){
			if(dnis.demo && dnis.demo === name) {
				console.log("setting " + dnis.demo + " phone number");
				$('span.phone-number.' + dnis.demo).html('<a href="tel:+1'+dnis.dnis+'">'+dnis.dnis+'</a>');
			}
		});
	}
}
//create main navigation tabs at the top of the page
function makeMainTab(tabName,pinnedTabs) {
	//tabLoad(tabName);
	var row = $('<div class="row"></div>');
	var col1 = $('<div class="col-sm-2"></div>');
	var col2 = $('<div class="col-sm-10 no-left-padding"></div>');
	var ul = $('<ul id="'+tabName+'-tabs" class="nav nav-pills nav-stacked nav-labconfig" data-tabs="tabs"></ul>');
	var div = $('<div id="'+tabName+'-tabs-content" class="tab-content"></div>');
	col1.append(ul);
	col2.append(div);
	row.append(col1).append(col2);
	$("#"+tabName).html(row);


	$.ajax(config.api.ui.tab(tabName))
	.done(function(tabs){
		console.log("tabs received: ",tabs);
		subTabLoad(tabName,tabs,pinnedTabs);
		tabLoad(tabName);
	})
	.fail(function(xhr){
		ajaxFailCheck(xhr);
		console.log("failed to get ui tab for '"+tabName+"': ",xhr);
	});
}

//var size = Object.size(myArray);

//Speed up calls to hasOwnProperty
var hasOwnProperty = Object.prototype.hasOwnProperty;

function formval(form,type,name) {
	return $(form).find(type+'[name="'+name+'"]').val()
}

function checkMembership(params) {
  
	function refresh() {
		$(params.selector).html(params.loadingText);
		$.ajax( params.checkMembershipUrl )
		.done(getProvisionDone)
		.fail(getProvisionFail);
	}

	function getProvisionFail(xhr, textStatus, errorThrown ) {
		switch(xhr.status) {
		case 401:
			$(params.selector).html(error401Text);
			break;
		case 404: // not found is a valid response, saying it didn't find the user
		  makeInviteButton();
      break;
		default:
			console.log("Error, web service failed. Response code " + xhr.status);
			$(params.selector).html(serverErrorText);
		  break;
		}
	}

	function makeInviteButton() {
	  var div = $(params.selector);
	  div.empty();
	  
    var button = $('<button type="button" class="btn btn-success">'+params.inviteButtonText+'</button>');
    div.append(params.noMessage).append('&nbsp;');
    div.append(button).append('&nbsp;');
    if(params.useModal) {
      button.click(openModal);
      $(params.modal).find('form').submit(function(e){
        e.preventDefault();
        saveData($(params.modal).find('form button[type="submit"]'));
      });
    } else {
      button.click(function(e){
        e.preventDefault();
        saveData(button);
      });
    }
	}
	
	function getProvisionDone(response) {
		var div = $(params.selector);
		

		if(response.status === 'success') {
			div.html(params.yesMessage);	
		} else {
		  makeInviteButton();
		} 
	} 

	function openModal(e) {
		e.preventDefault();
		$(params.modal).modal('show');
	}

	function saveData(button) {
		var status = button.data('status');
		switch(status) {
		case 'processing':
			break;
		default:
			saveDataPost(button);
		break;
		}
	};

	function saveDataPost(button) {
		var normalText = button.html();
		button.data('status', 'processing');
		button.html(params.workingText);
		console.log("starting ajax to " + params.inviteUrl);
		
		var data;
		if(typeof params.inviteData === 'function') {
		  console.log("params = ", params.inviteData());
		  data = JSON.stringify(params.inviteData());
		} else {
		  data = {};
		}
		$.ajax({
			url: params.inviteUrl,
			method: "POST",
			data: data
		})
		.done(function(rsp){
			if(params.useModal) {
//				new SuccessNotification(params.successMessage,params.modal).notify();
				try {
				  // attempt to hide the modal before displaying the success message to the main page
				  $(params.modal).modal("hide");
				} catch(e) {
				  // maybe the params.modal was incorrectly definded?
				  // do nothing on failure - the user should be able to clear any present modal by normal means
				  // and the success message should still be partially visible in the background of the modal
				}
			}
			// display success message to page now that the modal has been hidden (if any)
			new SuccessNotification(params.successMessage).notify();
			// get data from the server to provide user with latest status on the server 
			// whether our operations were actually successful or not - the server has the canonical info
			refresh();
		})
		.fail(function(xhr, textStatus, errorThrown ) {
			console.log("failed to post data for invitation/account creation: ",xhr);
			ajaxFailCheck(xhr);
			var responseMessage = "";
			try {
				responseMessage = xhr.responseJSON.responseMessage + " " + xhr.responseJSON.detail;
			} catch(e) {
				console.log("exception making failure notification message details");
				try {
					responseMessage = xhr.responseText;
				} catch(f) {}
			}
			var message = params.failureMessage + "\r\n Status " + xhr.status + " " + responseMessage;
			console.log(message);
			if(params.useModal) {
				new FailureNotification(message,params.modal).notify();
			} else {
				new FailureNotification(message).notify();
			}
		})
		.always(function(xhr) {
			// reset the button
			button.data('status', 'normal');
			button.html(normalText);
		});
	}

	return {
		refresh: function(){refresh(); return this;}
	}
} // checkMembership object

function singleDbForm(params) {
	var row = $('<tr></tr>');
	var keyCell = $('<td></td>');
	var valueCell = $('<td></td>');
	var actionCell = $('<td></td>');
	var input;
	// set up the row
	row.append(keyCell).append(valueCell).append(actionCell);
	// put the row into the table
	params.table.append(row);
	// display the key in the Agent column
	keyCell.append(params.key);

	// show loading in marquee column for this agent
	valueCell.append(params.loadingText);

	// call this to load the data and recreate the buttons
	function refresh() {
		// empty the action cell
		actionCell.empty();
		$.ajax( params.apiUrl )
		.done( loadDone )
		.fail( loadFail );
	}

	function loadFail(xhr, status, error) {
		valueCell.html(serverErrorText);
	}

	function saveHandler(event){
		// store this button's current text
		var originalText = $(this).html();
		// update button UI to show work in progress
		$(this).html(workingText);
		// reference back to 'this' for use inside the ajax methods
		var me = this;
		// prevent additional clicks while working
		var status = $(this).data('status');
		switch(status){
		case 'processing':
			return;
		default: /* First click */
			$(this).data('status', 'processing');
		var data = {};
		data[params.valueColumn] = input.val();
		// update
		$.ajax({
			url:params.apiUrl,
			method:"PUT",
			data:JSON.stringify(data)
		}).done(function(rsp){
			new SuccessNotification(params.successMessage + params.keyName).notify();
			// empty the action cell
			actionCell.empty();
			// refresh data
			refresh();
		}).fail(function(xhr,status,error){
			new FailureNotification(params.failureMessage + params.keyName).notify();
			// return button text to normal
			$(me).html(originalText);
			// reset button status
			$(me).data('status','normal');
		});
		break;
		} // button status switch
	} // saveHandler

	function deleteHandler(event){
		var originalText = $(this).html();
		// update button UI to show work in progress
		$(this).html(params.workingText);
		var me = this;
		// prevent additional clicks while working
		var status = $(this).data('status');
		switch(status){
		case 'processing':
			return;
		default: /* First click */
			$(this).data('status', 'processing');
		// delete value
		$.ajax({
			url:params.apiUrl,
			method:"DELETE"
		}).done(function(rsp){
			// alert the user
			new SuccessNotification(params.successMessage + params.keyName).notify();
			// empty the action cell
			actionCell.empty();
			// refresh data
			refresh();
		}).fail(function(xhr,status,error){
			// alert the user
			new FailureNotification(params.failureMessage + params.keyName).notify();
			// return button text to normal
			$(me).html(originalText);
			// reset button status
			$(me).data('status','normal');
		});
		break;
		}//switch
	} //deleteHandler

	// create input field and action buttons for the key/value
	function loadDone(rsp){
		console.log("rsp for "+params.key+": ",rsp);
		input = $('<input type="text" data-max-length="255" class="form-control" placeholder="'+params.placeholder+'" size="68" />');

		// does this response have data?
		if(!$.isEmptyObject(rsp)) {
			// fill in the current value
			input.val(rsp[params.valueColumn]);
			// give the user buttons to update or delete the current value
			var updateButton = $('<button type="button" class="btn btn-success">Update</button>');
			// handler for update button
			updateButton.on("click",saveHandler);
			// put the buttons in the cell
			actionCell.append(updateButton).append('&nbsp;');

			// give the user a reset button to reload the data from the server
			var resetButton = $('<button type="button" class="btn btn-info">Reset</button>');
			resetButton.on("click",function(e){refresh();}); // setButton.onclick
			// put the button in the cell
			actionCell.append(resetButton).append('&nbsp;');

			// the delete button
			var deleteButton = $('<button type="button" class="btn btn-danger">Delete</button>');
			deleteButton.on("click",deleteHandler);
			// put the buttons in the cell
			actionCell.append(deleteButton).append('&nbsp;');
		} else { // no data returned
			// give the user a button to set the data
			var setButton = $('<button type="button" class="btn btn-success">Set</button>');
			setButton.on("click",saveHandler); // setButton.onclick
			// put the button in the cell
			actionCell.append(setButton).append('&nbsp;');

			// give the user a reset button to reload the data from the server
			var resetButton = $('<button type="button" class="btn btn-info">Reset</button>');
			resetButton.on("click",function(e){refresh();}); // setButton.onclick
			// put the button in the cell
			actionCell.append(resetButton).append('&nbsp;');
		} // end if

		// replace loading text with input field
		valueCell.html(input);
	};

	// load data
	refresh();
}
/*
 * navigate to the url in the anchor and keep the current URL hash
 * @param selector optional, the jquery selector you want to apply this to. defaults to "a[rel~='keep-hash']" if input is undefined
 */
function keepHash(selector) {
	// default value
	if(typeof selector === 'undefined')
		selector =  "a[rel~='keep-hash']";
	$(selector).click(function(e) {
		e.preventDefault();
		var dest = $(this).attr('href') + window.location.hash;
		// a short timeout may help overcome browser bugs
		window.setTimeout(function() {
			window.location.href = dest;
		}, 100);
	});
}

function getRdpLink(params) {
	var server;
	if(typeof params === 'string')
		server = params;
	else 
		server = params.server;
	var username = params.username || cache.user.username;
	var domain = params.username || config.ldap.domain.name;
	return "rdp://full%20address=s:"+server+"&domain=s:"+domain+"&username=s:"+username+"&screen%20mode%20id=i:1&screen%20bpp=i:24&use%20multimon=i:0&audiomode=i:2";
}

var QueryString = function () {
	// This function is anonymous, is executed immediately and
	// the return value is assigned to QueryString!
	var query_string = {};
	var query = window.location.search.substring(1);
	var vars = query.split("&");
	for (var i=0;i<vars.length;i++) {
		var pair = vars[i].split("=");
		// If first entry with this name
		if (typeof query_string[pair[0]] === "undefined") {
			query_string[pair[0]] = decodeURIComponent(pair[1]);
			// If second entry with this name
		} else if (typeof query_string[pair[0]] === "string") {
			var arr = [ query_string[pair[0]], decodeURIComponent(pair[1]) ];
			query_string[pair[0]] = arr;
			// If third or later entry with this name
		} else {
			query_string[pair[0]].push(decodeURIComponent(pair[1]));
		}
	}
	return query_string;
} ();

//default instance is cxd (11.0 lab)
config.instance = QueryString.instance || 'cxd';
console.log("config.instance = " + config.instance);

function sectionLoad(page) {
	$("#section-"+page).load("tools/"+page+"/"+ config.instance + "-"+page+".html");
}
//load the sub-tabs on a page
function subTabLoad(tabName,tabs,pinnedTabs){
	var ul = '#'+tabName+'-tabs';
	$.each(tabs, function(i, tab) {
		var span = '';
		if(tab.icon.indexOf('<') == 0)
			span = $(tab.icon);
		else 
			span = $('<span></span>').addClass(tab.icon);
		var tags = '';
		// if tab is in beta
		if(typeof tab.beta !== 'undefined' && tab.beta == true) {
			tags += ' <span class="label label-info"> Beta</span>';
		}
		console.log("tab.created = " + tab.created);
		// if tab is new
		var tab_created = moment(tab.created);
		console.log("tab_created = " + tab_created);
		// get browser's current datetime
		var current_time = moment();
		console.log("current_time = " + current_time);

		var age = current_time.diff(tab_created, 'days');
		console.log("age = " + age);
		if(age < config.ui.newDays) {
			tags += ' <span class="label label-success"> New!</span>';
		}
		var a = $('<a></a>').attr('href', '#'+tabName+'-'+tab.folder).attr('data-toggle', 'tab').append(span).append(' ' + tab.name + tags);
		var li = $('<li></li>').addClass("text-nowrap").append(a);
		// put pinned tabs at the top
//		console.log("tab.folder = " + tab.folder);
//		console.log("pinnedTab = " +pinnedTabs[0]);
		if(Array.isArray(pinnedTabs) && pinnedTabs.indexOf(tab.folder) > -1) {
			console.log("prepending pinned tab " + tab.folder);
			$(ul).prepend(li);
		}
		else {
			$(ul).append(li);
		}
	});
	// make the first sub-tab active
	$(ul).find('li:first-child').addClass("active");
}
//load the sub-tabs on a page
function indexTabLoad(tabName,tabs){
	var ul = '#'+tabName+'-tabs';
	$.each(tabs, function(i, tab) {
		var span = '';
		if(tab.icon.indexOf('<') == 0)
			span = $(tab.icon);
		else 
			span = $('<span></span>').addClass(tab.icon);
		var tags = '';
		// if tab is in beta
		if(typeof tab.beta !== 'undefined' && tab.beta == true) {
			tags += ' <span class="label label-info"> Beta</span>';
		}
		console.log("tab.created = " + tab.created);
		// if tab is new
		var tab_created = moment(tab.created);
		console.log("tab_created = " + tab_created);
		// get browser's current datetime
		var current_time = moment();
		console.log("current_time = " + current_time);

		var age = current_time.diff(tab_created, 'days');
		console.log("age = " + age);
		if(age < config.ui.newDays) {
			tags += ' <span class="label label-success"> New!</span>';
		}
		var a = $('<a></a>').attr('href', '#'+tab.shortname).attr('data-toggle', 'tab').append(span).append(' ' + tab.name + tags);
		var li = $('<li></li>').addClass("text-nowrap").append(a);
		$(ul).append(li);

		//for each of these index-tabs, fire shown.bs.tab on the first child of the subtab list
		a.on('shown.bs.tab',function(e){
			$('#'+tab.shortname+'-tabs').find('li:first-child a').tab('show');
		});

	});
	$(ul).find('li:first-child').addClass("active");
}


//create tabs-content li for each <a> in the tabs list
function tabLoad(section) {
	// make sure the first sub-tab is active
	$('#'+section+'-tabs li:first-child').addClass("active");
	// add the tabs themselves into the page
	$.each($('#'+section+'-tabs li'),function(k,v) {
		var link = $(v).children('a').attr('href');
		//console.log("section children a = ",$(v).children('a'));
		//console.log("tab link = ",link);
		var contentDiv = $('<div></div>').addClass("tab-pane").attr("id",link.substring(1));
		if(k==0) {
			contentDiv.addClass("active");
		}
		$('#'+section+'-tabs-content').append(contentDiv);
		var prefix = '#'+section+'-';
		var len = prefix.length;
		var name = link.substring(len);
		//console.log("link name = ",name);
		//console.log("prefix+name = ",prefix+name);
//		var file = 'index/' + section + '/'+name+'.html';
		var file = 'tools/' + name + '/'+name+'.html';
		//console.log("file = ",file);
		$(prefix+name).load(file + '?nocache='+Date.now());
	});

	//store the currently selected tab in the hash value
	$("#" + section + "-tabs > li > a").on("shown.bs.tab", function(e) {
		var id = $(e.target).attr("href").substr(1);
		window.location.hash = id;
	});

	// on load of the page: switch to the currently selected tab
	var hash = window.location.hash;
	console.log("tabload for " + section);
	console.log("tabload hash = " + hash);
	// is the hash pointing to this section?
	if (hash.indexOf("#" + section + "-") == 0) {
		console.log("showing main tab #" + section);
		// show the main tab
		$('#index-tabs a[href="#' + section + '"]').tab('show');
		console.log("showing sub tab " + hash);
		// now show the sub-tab
		$('#' + section + '-tabs a[href="' + hash + '"]').tab('show');
	} else if(hash.indexOf("#"+section) == 0) {
		console.log("showing main tab #" + section);
		// show the main tab
		$('#index-tabs a[href="#' + section + '"]').tab('show');
	}
}

function activateTabsLinks(){
	// activate inter-page links
	$("a.tabs-link").on("click",function(){		
		// on load of the page: switch to the currently selected tab
		var hash = $(this).attr("href");
		var section = "";
		if(hash.indexOf("-") > 0) { // is there a subsection?
			section = hash.substring(1,hash.indexOf("-"));
			// show the main tab
			$('#index-tabs a[href="#' + section + '"]').tab('show');
			// now show the sub-tab
			$('#' + section + '-tabs a[href="' + hash + '"]').tab('show');
		} else { // link to a main tab
			section = hash.substring(1);
			// show the main tab
			$('#index-tabs a[href="#' + section + '"]').tab('show');
		}
	});
}

var adminRevealed = false;
function revealGroupUi(user) {

	// show admin interface items
	if(typeof user.groups.Admins != "undefined") {
		console.log("Admin user. Revealing all UI pieces.")
		$(".group-admin").show();
		$(".group-cisco").show();
		$(".group-egain").show();

		//if( ! adminRevealed ) {
		//$("#index-tabs").append('<li><a href="#admin" data-toggle="tab"><span class="glyphicon glyphicon-lock"></span> Admin</a></li>');
		//$('#index-tabs-content').append('<div class="tab-pane" id="admin"></div>');
		//$("#admin").load("index/admin.html");
		//adminRevealed = true;
		//}
	}
	if(typeof user.groups.Cisco != "undefined") {
		console.log("Cisco user. Revealing Cisco UI pieces.")
		$(".group-cisco").show();
	}
	if(typeof user.groups.eGain != "undefined") {
		console.log("eGain user. Revealing eGain UI pieces.")
		$(".group-egain").show();
		// hide content from egain users
		$(".group-not-egain").hide();
	}
	if(typeof user.groups.SpinSci != "undefined") {
		console.log("SpinSci user. Revealing SpinSci UI pieces.")
		$(".group-spinsci").show();
		// hide content from SpinSci users
		$(".group-not-spinsci").hide();
	}
	if(typeof user.groups["Bucher-Suter"] != "undefined") {
		console.log("Bucher-Suter user. Revealing Bucher-Suter UI pieces.")
		$(".group-bucher-suter").show();
		// hide content from SpinSci users
		$(".group-bucher-suter").hide();
	}
}

function checkJabberProvisioned(phones) {
	var csfFound = false;
	$.each(phones, function(k, phone) {
		// check for mobile agent extension and fill in those elements - they are located in other places on the site
		if(!csfFound && phone.name.indexOf("CSF") == 0){
			csfFound = true;
			$("#jabber-phone-provisioned").html('Your Jabber phone is provisioned with extension <strong>' + phone.dialed_number + '</strong>');
		}
	});
	// no mobile agent extension exists?
	if(!csfFound) {
		$("#jabber-phone-provisioned").html('Your Jabber phone is not provisioned. Provision your Jabber phone on the <a href="#phones-agent" class="tabs-link">Agent Phones</a> page.');
		activateTabsLinks();
	}
}
function checkLcpProvisioned(phones) {
	var found = false;
	$.each(phones, function(k, phone) {
		// check for mobile agent extension and fill in those elements - they are located in other places on the site
		if(!found && phone.name.indexOf("CSF") == 0){
			found = true;
			$(".finesse-mobile-agent-extension").html(phone.dialed_number);
		}
	});
	// no mobile agent extension exists?
	if(!found) {
		$(".finesse-mobile-agent-extension").html('You are not provisioned for Mobile Agent. Register your Mobile Agent Phone on the <a href="#phones-agent" class="tabs-link">Agent Phones</a> page.');
		activateTabsLinks();
	}
}

//generic form to database mapping. make the input elements text and name them matching using data names from the database
//you can name a button 'reset' and it will reload the information
function databaseForm(params) {
	// start out displaying loading message
	if(params.hideInitially){
		params.formGroup.hide();
		params.loadingDiv.html(params.loadingText).show();
	}
	params.form.unbind("submit");
	// set up submit button
	params.form.submit(function(event) {
		console.log("user pressed submit on " + params.form.attr('id'));
		var status = $(this).data('status');
		switch(status){
		case 'processing':
			event.preventDefault();
			break;
		default: /* First click */
			event.preventDefault();
		saveFormData();
		}
	});

	// set up reset button
	params.form.find('input[name=reset]').unbind("click");
	params.form.find('input[name=reset]').on("click",function(e){
		loadFormData();
	});

	// set up delete button
	params.form.find('input[name=delete]').unbind("click");
	params.form.find('input[name=delete]').on("click",function(e){
		deleteFormData();
	});

	function loadFormData() {
		// hide input form html before settings are loaded
		params.formGroup.hide();
		params.loadingDiv.html(params.loadingText).show();
		$.ajax(params.api)
		.done(function( rsp ) { 
			console.log(params.api + "response: ",rsp);
			params.loadingDiv.hide();
			params.formGroup.show();
			if(params.hasOwnProperty("type") && params.type === "key-value") {
				// find template row
//				var template = params.form.find(" input[name=key]").closest('tr');

				var tbody = params.form.find(" tbody");
				// empty table
				tbody.empty();
				// update input form with data
				$.each(rsp,function(k,v){
					var clone = $(params.template).first().clone();
					// fill template row
					clone.find(" input[name=key]").val(v.key);
					clone.find(" input[name=value]").val(v.value);
					tbody.append(clone);
				});
				activateDeleteRowButtons();
				// remove the template row from UI
//				tr.remove();
			} else {
				// update input form with data
				$.each(rsp,function(k,v){
					params.form.find(" input[name="+k+"]").val(v);
				});
			}
		})
		.fail(function(xhr, status, error) { 
			if(xhr.status == 401) {
				window.location.assign("/labconfig/");
			} else {
				params.loadingDiv.html(params.errorText);
			}
		});
		return this;
	}

	function activateDeleteRowButtons(){
		// activate Delete Row buttons
		params.form.find(" tbody").find(" button[name=delete-row]").on('click',function(e){
			$(this).closest('tr').remove();
		});
	}

	function saveFormData() {
		var data = {};
		if(params.hasOwnProperty("type") && params.type === "key-value") {
			var kvpairs = [];
			$.each(params.form.find("tbody > tr"),function(k,v){
				var key = $(v).find("input[name=key]").val();
				console.log("key",key);
				var value = $(v).find("input[name=value]").val();
				console.log("value",value);
				var element = {"key":key,"value":value};
				kvpairs.push(element);

			});
			console.log("kvpairs",kvpairs);
			data["params"] = kvpairs;
			data["id"] = params.key;
		} else {
			// fill data with input form data
			$.each(params.form.find("input[type=text]").not('[data-send="false"]'),function(k,v){
				data[$(v).attr('name')] = $(v).val();
			});
			$.each(params.form.find("textarea").not('[data-send="false"]'),function(k,v){
				data[$(v).attr('name')] = $(v).val();
			});
		}
		$.ajax({
			url: params.api,
			method: "PUT",
			contentType: "application/json",
			data: JSON.stringify(data)
		})
		.done(function( rsp ) {
//			console.log(rsp);
			if(isModal()){
				new SuccessNotification(params.successMessage,params.form).notify();
			} else {
				new SuccessNotification(params.successMessage).notify();
			}
//			params.getSuccessNotification().notify();
		})
		.fail(function(xhr, status, error) {
			console.log("xhr object:",xhr);
			if(xhr.status == 401) {
				window.location.assign("/labconfig/");
			}
			//notify the user that the action failed
//			new FailureNotification(params.failureMessage + $.parseJSON(xhr.responseText).detail).notify();
			if(xhr.hasOwnProperty('responseJSON')) {
				if(isModal()){
					new FailureNotification(params.failureMessage + xhr.responseJSON.detail, params.form).notify();
				} else {
					new FailureNotification(params.failureMessage + xhr.responseJSON.detail).notify();
				}
//				params.getFailureNotification(xhr.responseJSON.detail).notify();
			}
			else {
				if(isModal()){
					new FailureNotification(params.failureMessage + xhr.status, params.form).notify();
				} else {
					new FailureNotification(params.failureMessage + xhr.status).notify();
				}
//				params.getFailureNotification(xhr.status).notify();
			}
		})
		.always(function(rsp){
			loadFormData();
			params.form.data('status','normal');
		});
	}
	function isModal() {
		return params.hasOwnProperty("modal") && params.modal === true;
	}
	function deleteFormData() {
		$.ajax({
			url: params.api,
			method: "DELETE"
		})
		.done(function( rsp ) {
//			console.log(rsp);
//			params.getSuccessNotification().notify();
			if(isModal()) {
				params.form.modal("hide");
				params.reload();
			}
			new SuccessNotification(params.successMessage).notify();
		})
		.fail(function(xhr, status, error) {
			console.log("xhr object:",xhr);
			//notify the user that the action failed
//			new FailureNotification(params.failureMessage + $.parseJSON(xhr.responseText).detail).notify();
			if(xhr.hasOwnProperty('responseJSON')) {
				if(isModal()){
					new FailureNotification(params.failureMessage + xhr.responseJSON.detail,params.form).notify();
				} else {
					new FailureNotification(params.failureMessage + xhr.responseJSON.detail).notify();
				}
//				params.getFailureNotification(xhr.responseJSON.detail).notify();
			}
			else {
				if(isModal()){
					new FailureNotification(params.failureMessage + xhr.status, params.form).notify();
				} else {
					new FailureNotification(params.failureMessage + xhr.status).notify();
				}
//				params.getFailureNotification(xhr.status).notify();
			}
		})
		.always(function(rsp){
			params.form.data('status','normal');
		});
	}

	return {
		load: loadFormData
		,activateDeleteRowButtons: activateDeleteRowButtons
	};

}

function dragAndDrop(params) {
	params.form.find("progress").hide();
//	params.form.find("progress").progressbar();

	params.form.find('input[type=file]').on("change",function(e){
		var file = params.form.find('input[type=file]')[0].files[0];
		console.log('user selected file: ',file);
//		params.form.find(".file-name").html(file.name);
		readfiles(params.form.find('input[type=file]')[0].files);
	});

	params.dnd.on("dragover",function () { 
		$(this).addClass('hover'); return false; 
	});
	params.dnd.on("dragend", function () {
		$(this).removeClass('hover'); return false;
	});
	params.dnd.on("drop",function (e) {
		$(this).removeClass('hover');
		e.preventDefault();
		readfiles(e.originalEvent.dataTransfer.files);
	});
	params.dnd.on("click",function (e) {
		params.form.find('input[type=file]').click();
		return false;
	});

	var registeredFiles = [];
	var originalDndText = params.dnd.html();
	function registerFile(file){
		console.log("registering file " + file);
		registeredFiles.push(file);
	}
	function deregisterFile(file) {
		console.log("deregistering file " + file);
		var index = registeredFiles.indexOf(file);
		if (index > -1) {
			registeredFiles.splice(index, 1);
		}
		if(registeredFiles.length == 0){
			//all done!
			params.dnd.html(originalDndText);
		}
	}

	function readfiles(files) {
		params.dnd.html(params.dndWorkingText);
		var fileNames = [];
		for (var i = 0; i < files.length; i++) {
			sendData(files[i]);
			console.log("sending file: ",files[i].name);
			registerFile(files[i].name);
		}
	}

	params.form.on("submit",function(event) {
		var status = $(this).data('status');
		switch(status){
		case 'processing':
			event.preventDefault();
			break;
		default: /* First click */
			event.preventDefault();
		readfiles(params.form.find('input[type=file]')[0].files);
		}
	});


	function sendData(file) {

		if (file.size > params.sizeLimit * 1024 * 1024) {
			params.fileTooLargeNotification.notify();
			return;
		}
		$(this).data('status', 'processing');
		var originalText = params.dnd.html();
		params.form.find('input[type=submit]').html(workingText);

		var formData = new FormData();
		formData.append('file', file);
		// fill data with input form data
		$.each(params.form.find("input[type=text],select"),function(k,v){
			formData.append($(v).attr('name'), $(v).val());
		});
		$.each(params.form.find("input[type=checkbox]"),function(k,v){
			formData.append($(v).attr('name'), $(v).is(':checked'));
		});

		console.log('uploading file to url: ' + params.url);
		$.ajax({
			method: params.method,
			url: params.url,
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
		}).done(function(rsp) {
			console.log("file upload success",rsp);
			new SuccessNotification(params.successMessage).notify();
			console.log("running refresh handlers");
			if(typeof params.refreshHandlers !== 'undefined') {
//				register.viqTable.refresh();
				$.each(params.refreshHandlers,function(i,handler){
					try{
						handler();
					} catch(e){
						console.log("exception trying to execute handler: ",handler);
					}
				});
			}
		}).fail(function(xhr,status,error){
			console.log(xhr);
			if(xhr.status == 401) {
				window.location.reload();
			}
			if(xhr.hasOwnProperty('responseJSON'))
				new FailureNotification(params.failureMessage + xhr.responseJSON.detail).notify();
			else 
				new FailureNotification(params.failureMessage + xhr.status).notify();
		}).always(function(xhr, status, error) {
			params.form.data('status','normal');
			params.dnd.html(originalText);
			deregisterFile(file.name);
		});
	}
}

function stringFormat(string, replacements) {
	//var replacements = {"%USERID%":user.userid,"%INSTANCE%":config.instance};

	return string.replace(/{\w+}/g, function(all) {
		return replacements[all] || all;
	});
}



function isEmpty(obj) {

	// null and undefined are "empty"
	if (obj == null) return true;

	// Assume if it has a length property with a non-zero value
	// that that property is correct.
	if (obj.length > 0)    return false;
	if (obj.length === 0)  return true;

	// Otherwise, does it have any properties of its own?
	// Note that this doesn't handle
	// toString and valueOf enumeration bugs in IE < 9
	for (var key in obj) {
		if (hasOwnProperty.call(obj, key)) return false;
	}

	return true;
}

//store the currently selected tab in the hash value
$("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
	var id;
	try {
		id = $(e.target).attr("href").substr(1);
		window.location.hash = id;
	} catch(e) {
		console.log("exception trying to change hash when nav-tabs tab link was clicked : ",e);
	}
});

//on load of the page: switch to the currently selected tab
//var hash = window.location.hash;
//$('#index-tabs a[href="' + hash + '"]').tab('show');

function loadElements(elements) {
	$.each(elements,function(k,v) {
		var data = v[config.instance];
		if(typeof data === 'string') {
			$(k).append(data);
		} else if(typeof data === 'object') {
			$.each(data,function(j,row) {
				$(k).append(row);
			});
		}
	});
}

function shortenUrl(longUrl,callback) {
	var xhr = new XMLHttpRequest();
	var bitlyLogin = config.demo.tropo.bitly.login;
	var bitlyApiKey = config.demo.tropo.bitly.apiKey;
	var bitlyApiUrl = "";
	// use different bitly API URLs depending on current protocol
	if (window.location.protocol === "https:")
		bitlyApiUrl = "https://api-ssl.bit.ly/v3/shorten";
	else // http:
		bitlyApiUrl = "http://api.bit.ly/v3/shorten";
	var bitlyUrl = bitlyApiUrl + "?format=json&login="
	+ bitlyLogin + "&apiKey=" + bitlyApiKey
	+ "&longUrl=" + encodeURIComponent(longUrl);
	xhr.open("GET", bitlyUrl);
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4) {
			console.log("shortenUrl: bitly returned status " + xhr.status);
			if (xhr.status == 200) {
				var shortUrl = JSON.parse(xhr.response).data.url;
				console.log("shortened URL = ",shortUrl);
				console.log("entire bitly response = ",xhr.response);
				callback(shortUrl);
			} else {
				console.log("Oops", xhr);
				reset();
				new FailureNotification("Couldn't create short URL. SMS not sent.").notify();
			}
		}
	}
	xhr.send();
}
function shortenUrl2(longUrl,callback) {
	var xhr = new XMLHttpRequest();
	var apiKey = "l1mrf02vop6dkf3qjc6a2s016k7isf5mduqcs91e2k1slon8lh2ln6litlkgi5cmdiqcv8e4tivdgkpdas9kimgkul42pleh2dg04ol2g0gr";
	var apiPath = "//api.cxdemo.net/create-link";
	var apiUrl = apiPath + "?token="+ apiKey
	+ "&longUrl=" + encodeURIComponent(longUrl);
	xhr.open("GET", apiUrl);
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4) {
			console.log("shortenUrl: toolbox.cxdemo.net returned status " + xhr.status);
			if (xhr.status == 200) {
				var shortUrl = JSON.parse(xhr.response).url;
				console.log("shortened URL = ",shortUrl);
				console.log("entire toolbox.cxdemo.net response = ",xhr.response);
				callback(shortUrl);
			} else {
				console.log("Oops", xhr);
				//reset();
				new FailureNotification("Couldn't create short URL: " + xhr.response).notify();
			}
		}
	}
	xhr.send();
}

function sendSms(ani,message) {
	console.log("sms number: ",ani);
	console.log("sms message: ",message);
	var tropoToken = config.demo.tropo.apiToken;
	var tropoApiUrl = "https://api.tropo.com/1.0/sessions";
	var smsUrl = tropoApiUrl + "?action=create&token="
	+ tropoToken + "&ani=" + ani + "&msg=" + message;

	var xhrSms = new XMLHttpRequest();
	xhrSms.open("GET", smsUrl);
	xhrSms.onreadystatechange = function() {
		if (xhrSms.readyState == 4) {
			if (xhrSms.status == 200) {
				console.log(xhrSms.response);
				//reset();
				new SuccessNotification("SMS sent successfully.").notify();
			} else {
				console.log("Oops", xhrSms);
				reset();
				new FailureNotification("Failed to send SMS.").notify();
			}
		}
	}
	xhrSms.send();
}

/**
 * Right-click context menu
 */
(function ($, window) {

	$.fn.contextMenu = function (settings) {

		return this.each(function () {

			// Open context menu
			$(this).on("contextmenu", function (e) {
				// return native menu if pressing control
				if (e.ctrlKey) return;

				//open menu
				var $menu = $(settings.menuSelector)
				.data("invokedOn", $(e.target))
				.show()
				.css({
					position: "absolute",
					left: getMenuPosition(e.clientX, 'width', 'scrollLeft'),
					top: getMenuPosition(e.clientY, 'height', 'scrollTop')
				})
				.off('click')
				.on('click', 'a', function (e) {
					$menu.hide();

					var $invokedOn = $menu.data("invokedOn");
					var $selectedMenu = $(e.target);

					settings.menuSelected.call(this, $invokedOn, $selectedMenu);
				});

				return false;
			});

			//make sure menu closes on any click
			$(document).click(function () {
				try{
					$(settings.menuSelector).hide();
				} catch(e) {
					console.log("exception: ",e);
				}
			});
		});

		function getMenuPosition(mouse, direction, scrollDir) {
			var win = $(window)[direction](),
			scroll = $(window)[scrollDir](),
			menu = $(settings.menuSelector)[direction](),
			position = mouse + scroll;


			// If context-menu's parent is positioned using absolute or relative positioning,
			// the calculated mouse position will be incorrect.
			// Adjust the position of the menu by its offset parent position.
			var parentOffset = $(settings.menuSelector).offsetParent().offset();
			if(direction === "width")
				position = position - parentOffset.left - 4;
			else if(direction === "height")
				position = position - parentOffset.top - 4;				

			// opening menu would pass the side of the page
			if (mouse + menu > win && menu < mouse) 
				position = position - menu + 4;

			return position;
		}

	};

	$(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
		event.preventDefault();
		$(this).ekkoLightbox();
	});

	var version;
	var otherVersion;
	if(config.instance === 'cis'){
		version = '10.5';
		otherVersion = '11.0';
	}
	else if(config.instance === 'cxd'){
		version = '11.0';
		otherVersion = '10.5';
	}
	document.title = version + ' ' + document.title;

//	$(".instance-other-version").html(otherVersion);
})(jQuery, window);
